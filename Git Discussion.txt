Git VCS

What is Git VCS?
    - Git VCS is an open source version control system. 
    It allows us to track and update our files with only
    the changes made to it.

Gitlab vs Github
    - There is not much difference between the two.
    Both are just competing cloud services and manage our online repositories.

Two types of Repositories
    - Local Repository
        - folders that use git technology. Therefore it allows us 5o make changes 
        in the files with the folder. These changes can be uploaded and updated
        in our remote repo.
    
    - Remote Repository
        - these are the folders that use git technology but instead
        it is located in the internet or in cloud services such as Gitlab
        and Github.

Use SSH key for local and remote communications.

WHAT IS AN SSH KEY?
     - SSH or Secure Shell Key are tools we use to authenticate the uploading 
     of other tasks when manipulating or using git repo.
    - it allows us to push or simply upload changes to our repos without the need
    of password.



Basic Git Commands

git init 
    - it allows us to initiate a folder as a local remote repository

git status 
    - allows us to display or verify files that are ready to be added
    and then committed 

git add .
    - it allows us to track all the changes we have made and prepare
    these files as a new version to be uploaded.

git commit -m <"commit message">
    - allows us to create a new commit or version of our file to be 
    pushed into our remote repositories.

git remote add <aliasOfRemote> <urlOfRemote>
    - it allows us to add/commit a remote repositories to our local
    repo.

git remote -v
    - allows us to view the remote repositories connected to the local
    repo.

git push <alias> master
    - it allows us to push our updates/changes/version commit into
    our local repository

Git Configuration
    - these commands will allows us to have git recognize the person
    trying to push into an online remote repository

    git config --global.user.email "emailUsedInGitlab"
        - configure the email used to push into the remote repo.
    
    git config --global user.name "userNameUsedInGitlab"
        - configure the username/name used to push into GitLab

    git config --global.list
        - checks whether the user information has been successfully added.

    git log
        - it allows us to check the version history and ID of repo.



        
